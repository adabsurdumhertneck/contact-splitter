package data.sqlite;

import data.Contact;
import data.IDataRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SQLiteRepositoryTest {

    private IDataRepository repository;

    @Before
    public void init() {
        this.repository = SQLiteRepository.getInstance();
    }

    @Test
    public void testGetInstance() {
        Assert.assertNotNull(SQLiteRepository.getInstance());
    }

    @Test(expected = ContactNotCompleteException.class)
    public void testAddIncompleteContact() throws ContactNotCompleteException {
        Contact contact = new Contact();
        this.repository.addContact(contact);
    }

}
