package controller;

import data.Contact;
import data.Gender;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ContactSplitterTest {

    private ContactSplitter contactSplitter;
    private String input1;
    private Contact contact1;
    private String input2;
    private Contact contact2;
    private String input3;
    private Contact contact3;
    private String input4;
    private Contact contact4;
    private String input5;
    private Contact contact5;

    @BeforeEach
    void setUp() {
        contactSplitter = new ContactSplitter();
        input1 = "Dr. Russwurm, Winfried";
        input2 = "Mme. Chloé Dubois";
        input3 = "Maud Solveig Christina Wikström";
        input4 = "Mrs. Doreen Faber";
        input5 = "Herr Dr.-Ing. Dr. rer. nat. Dr. h.c. mult. Paul Steffens";

        //contact 1
        contact1 = new Contact();
        contact1.setFirstName("Winfried");
        contact1.setLastName("Russwurm");
        contact1.setGender(Gender.MALE);
        contact1.setLanguage("Deutsch");
        List<String> titleList1 = new ArrayList<>();
        titleList1.add("Dr.");
        contact1.setTitleList(titleList1);
        contact1.setStandardSalutation("Sehr geehrter Herr Dr. Winfried Russwurm");

        //contact 2
        contact2 = new Contact();
        contact2.setFirstName("Chloé");
        contact2.setLastName("Dubois");
        contact2.setGender(Gender.FEMALE);
        contact2.setLanguage("Französisch");
        List<String> titleList2 = new ArrayList<>();
        contact2.setTitleList(titleList2);
        contact2.setStandardSalutation("Chere Madame Chloé Dubois");

        //contact 3
        contact3 = new Contact();
        contact3.setFirstName("Maud Solveig Christina");
        contact3.setLastName("Wikström");
        contact3.setGender(Gender.FEMALE);
        contact3.setLanguage("Schwedisch");
        List<String> titleList3 = new ArrayList<>();
        contact3.setTitleList(titleList3);
        contact3.setStandardSalutation("Kära fru Maud Solveig Christina Wikström");

        //contact 4
        contact4 = new Contact();
        contact4.setFirstName("Doreen");
        contact4.setLastName("Faber");
        contact4.setGender(Gender.FEMALE);
        contact4.setLanguage("Englisch");
        List<String> titleList4 = new ArrayList<>();
        contact4.setTitleList(titleList4);
        contact4.setStandardSalutation("Dear Madam Doreen Faber");

        //contact 5
        contact5 = new Contact();
        contact5.setFirstName("Paul");
        contact5.setLastName("Steffens");
        contact5.setGender(Gender.MALE);
        contact5.setLanguage("Deutsch");
        List<String> titleList5 = new ArrayList<>();
        titleList5.add("Dr.-Ing.");
        titleList5.add("Dr. Rer. Nat.");
        titleList5.add("Dr. H.C. Mult.");
        contact5.setTitleList(titleList5);
        contact5.setStandardSalutation("Sehr geehrter Herr Dr.-Ing. Dr. Rer. Nat. Dr. H.C. Mult. Paul Steffens");
    }

    @Test
    void splitContactSuccessful1() {
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input1);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(contact1.getFirstName(), contact.getFirstName());
        Assert.assertEquals(contact1.getLastName(), contact.getLastName());
        Assert.assertEquals(contact1.getGender(), contact.getGender());
        Assert.assertEquals(contact1.getLanguage(), contact.getLanguage());
        Assert.assertEquals(contact1.getStandardSalutation(), contact.getStandardSalutation());
        Assert.assertEquals(contact1.getTitleList(), contact.getTitleList());
    }

    @Test
    void splitContactSuccessful2() {
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input2);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(contact2.getFirstName(), contact.getFirstName());
        Assert.assertEquals(contact2.getLastName(), contact.getLastName());
        Assert.assertEquals(contact2.getGender(), contact.getGender());
        Assert.assertEquals(contact2.getLanguage(), contact.getLanguage());
        Assert.assertEquals(contact2.getStandardSalutation(), contact.getStandardSalutation());
        Assert.assertEquals(contact2.getTitleList(), contact.getTitleList());
    }

    @Test
    void splitContactSuccessful3() {
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input3);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(contact3.getFirstName(), contact.getFirstName());
        Assert.assertEquals(contact3.getLastName(), contact.getLastName());
        Assert.assertEquals(contact3.getGender(), contact.getGender());
        Assert.assertEquals(contact3.getLanguage(), contact.getLanguage());
        Assert.assertEquals(contact3.getStandardSalutation(), contact.getStandardSalutation());
        Assert.assertEquals(contact3.getTitleList(), contact.getTitleList());
    }

    @Test
    void splitContactSuccessful4() {
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input4);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(contact4.getFirstName(), contact.getFirstName());
        Assert.assertEquals(contact4.getLastName(), contact.getLastName());
        Assert.assertEquals(contact4.getGender(), contact.getGender());
        Assert.assertEquals(contact4.getLanguage(), contact.getLanguage());
        Assert.assertEquals(contact4.getStandardSalutation(), contact.getStandardSalutation());
        Assert.assertEquals(contact4.getTitleList(), contact.getTitleList());
    }

    @Test
    void splitContactSuccessful5() {
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input5);
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals(contact5.getFirstName(), contact.getFirstName());
        Assert.assertEquals(contact5.getLastName(), contact.getLastName());
        Assert.assertEquals(contact5.getGender(), contact.getGender());
        Assert.assertEquals(contact5.getLanguage(), contact.getLanguage());
        Assert.assertEquals(contact5.getStandardSalutation(), contact.getStandardSalutation());
        Assert.assertEquals(contact5.getTitleList(), contact.getTitleList());
    }
}