package frontend;

import data.Gender;
import javafx.beans.property.*;
import javafx.collections.ObservableList;

/**
 * Holds the data that is shown in the GUI using JavaFX Property Binding
 * @author Robert Metzinger
 */
public class ViewModel {

    //Properties

    private StringProperty salutation = new SimpleStringProperty();
    private StringProperty standardSalutation = new SimpleStringProperty();
    private StringProperty title = new SimpleStringProperty();
    private StringProperty title2 = new SimpleStringProperty();
    private StringProperty title3 = new SimpleStringProperty();
    private StringProperty nobilityTitle = new SimpleStringProperty();
    private ObjectProperty<Gender> gender = new SimpleObjectProperty<Gender>();
    private StringProperty firstName = new SimpleStringProperty();
    private StringProperty lastName = new SimpleStringProperty();
    private StringProperty language = new SimpleStringProperty();
    private ListProperty<String> titles = new SimpleListProperty<String>();
    private ListProperty<String> noblilityTitles = new SimpleListProperty<String>();
    private ListProperty<String> languages = new SimpleListProperty<String>();


    //Getter and Setter

    public String getSalutation() {
        return salutation.get();
    }

    public void setSalutation(String salutation) {
        this.salutation.set(salutation);
    }

    public StringProperty salutationProperty() {
        return salutation;
    }

    public String getStandardSalutation() {
        return standardSalutation.get();
    }

    public void setStandardSalutation(String standardSalutation) {
        this.standardSalutation.set(standardSalutation);
    }

    public StringProperty standardSalutationProperty() {
        return standardSalutation;
    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getTitle2() {
        return title2.get();
    }

    public void setTitle2(String title2) {
        this.title2.set(title2);
    }

    public StringProperty title2Property() {
        return title2;
    }

    public String getTitle3() {
        return title3.get();
    }

    public void setTitle3(String title3) {
        this.title3.set(title3);
    }

    public StringProperty title3Property() {
        return title3;
    }

    public String getNobilityTitle() {
        return nobilityTitle.get();
    }

    public void setNobilityTitle(String nobilityTitle) {
        this.nobilityTitle.set(nobilityTitle);
    }

    public StringProperty nobilityTitleProperty() {
        return nobilityTitle;
    }

    public Gender getGender() {
        return gender.get();
    }

    public void setGender(Gender gender) {
        this.gender.set(gender);
    }

    public ObjectProperty<Gender> genderProperty() {
        return gender;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getLanguage() {
        return language.get();
    }

    public void setLanguage(String language) {
        this.language.set(language);
    }

    public StringProperty languageProperty() {
        return language;
    }

    public ObservableList<String> getTitles() {
        return titles.get();
    }

    public void setTitles(ObservableList<String> titles) {
        this.titles.set(titles);
    }

    public ListProperty<String> titlesProperty() {
        return titles;
    }

    public ObservableList<String> getNoblilityTitles() {
        return noblilityTitles.get();
    }

    public void setNoblilityTitles(ObservableList<String> noblilityTitles) {
        this.noblilityTitles.set(noblilityTitles);
    }

    public ListProperty<String> noblilityTitlesProperty() {
        return noblilityTitles;
    }

    public ObservableList<String> getLanguages() {
        return languages.get();
    }

    public void setLanguages(ObservableList<String> languages) {
        this.languages.set(languages);
    }

    public ListProperty<String> languagesProperty() {
        return languages;
    }
}
