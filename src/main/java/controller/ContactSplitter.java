package controller;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.errors.APIError;
import com.optimaize.anythingworks.common.host.Host;
import com.optimaize.command4j.CommandExecutor;
import com.optimaize.command4j.Mode;
import data.Contact;
import data.Gender;
import org.nameapi.client.lib.NameApiModeFactory;
import org.nameapi.client.lib.NameApiPortUrlFactory;
import org.nameapi.client.lib.NameApiRemoteExecutors;
import org.nameapi.client.services.parser.personnameparser.PersonNameParserCommand;
import org.nameapi.ontology5.input.context.Context;
import org.nameapi.ontology5.input.context.ContextBuilder;
import org.nameapi.ontology5.input.context.Priority;
import org.nameapi.ontology5.input.entities.person.InputPerson;
import org.nameapi.ontology5.input.entities.person.NaturalInputPersonBuilder;
import org.nameapi.ontology5.input.entities.person.name.InputPersonName;
import org.nameapi.ontology5.input.entities.person.name.builder.NameBuilders;
import org.nameapi.ontology5.output.entities.person.name.Term;
import org.nameapi.ontology5.services.parser.personnameparser.ParsedPerson;
import org.nameapi.ontology5.services.parser.personnameparser.ParsedPersonMatch;
import org.nameapi.ontology5.services.parser.personnameparser.PersonNameParserResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;

/**
 * Splits a contact into its components using name parsing and language detection.
 * Used APIs are nameapi.org and detectlanguage.com
 * @author Robert Metzinger
 */
class ContactSplitter {

    /**
     * Will be used if the language can not be detected or is not suported
     */
    private static final String defaultLanguage = "Deutsch";

    /**
     * Splits a contact into its components using name parsing and language detection.
     * @param input User input representing a contact/person that will be splitted into its components
     * @return holds the extracted data from the input
     * @throws Exception thrown when nameapi.org is not available
     */
    Contact splitContact(String input) throws Exception {

        String rawName = removeSalutation(input);
        Contact contact = new Contact();
        Context context = new ContextBuilder()
                .priority(Priority.REALTIME)
                .place("DE")
                .build();

        CommandExecutor executor = NameApiRemoteExecutors.get();
        Mode mode = NameApiModeFactory.withContext(
                "5d522aca2bd2553680e50531b53826fd-user1",
                context,
                //the default and live server is "api.nameapi.org"
                //we're using the latest release candidate with latest features here:
                new Host("rc50-api.nameapi.org", 80), NameApiPortUrlFactory.versionLatestStable()
        );
        InputPersonName name = NameBuilders.western().fullname(rawName).build();
        InputPerson inputPerson = new NaturalInputPersonBuilder().name(name).build();
        PersonNameParserCommand command = new PersonNameParserCommand();
        PersonNameParserResult result;
        result = executor.execute(command, mode, inputPerson).get();
        ParsedPersonMatch match = result.getBestMatch();
        ParsedPerson person = match.getParsedPerson();
        System.out.println(person.toString());
        System.out.println(result.toString());
        String fullFirstName = "";
        String fullLastName = "";
        if (person.getGender() != null) {
            switch (person.getGender().getGender()) {
                case MALE:
                    contact.setGender(Gender.MALE);
                    break;
                case FEMALE:
                    contact.setGender(Gender.FEMALE);
                    break;
                case NEUTRAL:
                    contact.setGender(Gender.OTHER);
                    break;
                default:
                    break;
            }
        }
        for (Term term : person.getOutputPersonName().getTerms()) {
            switch (term.getTermType()) {
                case SALUTATION:
                    contact.setSalutation(term.getString());
                    break;
                case TITLE:
                    contact.getTitleList().add(term.getString());
                    break;
                case GIVENNAMEABBREVIATION:
                    contact.getTitleList().set(contact.getTitleList().size() - 1, contact.getTitleList().get(contact.getTitleList().size() - 1).concat(" ").concat(term.getString()));
                    break;
                case GIVENNAME:
                    fullFirstName = fullFirstName.concat(term.getString()).concat(" ");
                    break;
                case MIDDLENAME:
                    fullFirstName = fullFirstName.concat(term.getString()).concat(" ");
                    break;
                case NICKNAME:
                    fullFirstName = fullFirstName.concat(term.getString()).concat(" ");
                    break;
                case GIVENNAMEINITIAL:
                    fullFirstName = fullFirstName.concat(term.getString()).concat(" ");
                    break;
                case SURNAME:
                    fullLastName = fullLastName.concat(term.getString()).concat(" ");
                    break;
                case OTHERSURNAME:
                    fullLastName = fullLastName.concat(term.getString()).concat(" ");
                    break;
                case SUFFIX:
                    fullLastName = fullLastName.concat(term.getString()).concat(" ");
                    break;
            }
        }
        contact.setFirstName(fullFirstName.trim().replaceAll(" +", " "));
        contact.setLastName(fullLastName.trim().replaceAll(" +", " "));
        String language = detectLanguage(input);
        contact.setLanguage(language);
        if (contact.getGender() != null) {
            String standardSalutation = SalutationGenerator.generate(contact);
            contact.setStandardSalutation(standardSalutation);
        }
        return contact;
    }

    /**
     * Removes a salutation from the name input if existent. The returned raw name will be given to the name parser. This ensures better results.
     * @param nameInput User input representing a contact/person that will be splitted into its components
     * @return the name input without any salutation
     */
    private String removeSalutation(String nameInput) {
        String rawName = nameInput;
        String filePath = "dictionary.properties";
        try (InputStream input = new FileInputStream(filePath)) {
            // load the properties file and get the values
            Properties prop = new Properties();
            prop.load(this.getClass().getClassLoader().getResourceAsStream(filePath));
            Collection<String> salutations = (Collection<String>) (Collection<?>) prop.values();
            for (String sal : salutations) {
                String[] arrSalutations = sal.split(",");
                for (String s : arrSalutations) {
                    if (nameInput.toLowerCase().contains(s)) {
                        rawName = nameInput.toLowerCase().replaceFirst(s, "");
                        return rawName;
                    }
                }
            }
        } catch (IOException ex) {
            return nameInput;
        }
        return rawName;
    }

    /**
     * Detects the language of the user input using the API detectlanguage.com
     * @param nameInput user input which is used to detect the language
     * @return the detected language
     */
    private String detectLanguage(String nameInput) {

        String language = "";

        //detect language via API detectlanguage.com
        DetectLanguage.apiKey = "78f25ea6c88fb45ed408540150d7f23c";
        String languageKey = "";
        try {
            languageKey = DetectLanguage.simpleDetect(nameInput);

        } catch (APIError apiError) {
            apiError.printStackTrace();
        }

        String filePath = "lang.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            Properties prop = new Properties();

            // load a properties file and get the values
            prop.load(this.getClass().getClassLoader().getResourceAsStream(filePath));
            language = prop.getProperty(languageKey, defaultLanguage);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return language;
    }
}
