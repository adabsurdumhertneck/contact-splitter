package controller;

import data.Contact;
import data.Gender;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Generator for the language and gender specific salutation which can be used e.g. in a newsletter
 * @author Robert Metzinger
 */
class SalutationGenerator {

    /**
     * Will be used if detected language is not supported
     */
    private final static String defaultSalutations = "Sehr geehrter Herr,Sehr geehrte Frau";

    /**
     * generates a salutation
     * @param contact Its data is used to generate the salutation
     * @return The generated salutation
     */
    static String generate(Contact contact) {
        String salutation = "";
        String salutations;
        String language = contact.getLanguage();
        Gender gender = contact.getGender();

        String filePath = "salutations.properties";
        try (InputStream input = new FileInputStream(filePath)) {
            // load the properties file and get the values
            Properties prop = new Properties();
            prop.load(SalutationGenerator.class.getClassLoader().getResourceAsStream(filePath));
            salutations = prop.getProperty(language, defaultSalutations);
        } catch (IOException | NullPointerException ex) {
            salutations = defaultSalutations;
        }
        String[] arrSalutations = salutations.split(",");
        if (gender != null) {
            switch (gender) {
                case MALE:
                    salutation = arrSalutations[0];
                    break;
                case FEMALE:
                    salutation = arrSalutations[1];
                    break;
                default:
                    salutation = arrSalutations[0];
                    break;
            }
        }
        if (contact.getTitleList() != null && contact.getTitleList().size() > 0) {
            salutation = salutation.concat(" ").concat(String.join(" ", contact.getTitleList()));
        }
        if (contact.getFirstName() != null) {
            salutation = salutation.concat(" ").concat(contact.getFirstName());
        }
        if (contact.getLastName() != null) {
            salutation = salutation.concat(" ").concat(contact.getLastName());
        }
        salutation = salutation.trim().replaceAll(" +", " ");
        return salutation;
    }
}
