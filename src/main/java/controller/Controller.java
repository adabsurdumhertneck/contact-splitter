package controller;

import data.Contact;
import data.Gender;
import data.IDataRepository;
import data.sqlite.ContactNotCompleteException;
import data.sqlite.SQLiteRepository;
import frontend.ViewModel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * The Controller for the JavaFX GUI
 *
 * @author Robert Metzinger
 */
public class Controller implements Initializable {

    @FXML
    private GridPane gridPane;
    @FXML
    private TextField txfContactInput;
    @FXML
    private TextField txfSalutation;
    @FXML
    private TextField txfStandardSalutation;
    @FXML
    private TextField txfFirstName;
    @FXML
    private TextField txfLastName;
    @FXML
    private ComboBox<Gender> cbxGender;
    @FXML
    private ComboBox<String> cbxLanguage;
    @FXML
    private ComboBox<String> cbxTitle;
    @FXML
    private ComboBox<String> cbxTitle2;
    @FXML
    private ComboBox<String> cbxTitle3;
    @FXML
    private Button btnAddThirdTitleMenu;
    @FXML
    private Button btnRemoveSecondTitle;
    @FXML
    private Button btnRemoveThirdTitle;

    private ContactSplitter contactSplitter;
    private ViewModel viewModel;
    private IDataRepository dataRepository;

    /**
     * Is executed when starting the app. Initializes UI Elements and sets bindings.
     *
     * @param location
     * @param resources
     */
    public void initialize(URL location, ResourceBundle resources) {

        contactSplitter = new ContactSplitter();
        viewModel = new ViewModel();
        dataRepository = SQLiteRepository.getInstance();

        //Initialize Comboboxes
        initCbxGender();
        initCbxLanguages();
        initCbxTitles();

        //Set Bindings
        txfSalutation.textProperty().bindBidirectional(viewModel.salutationProperty());
        txfStandardSalutation.textProperty().bindBidirectional(viewModel.standardSalutationProperty());
        txfFirstName.textProperty().bindBidirectional(viewModel.firstNameProperty());
        txfLastName.textProperty().bindBidirectional(viewModel.lastNameProperty());
        cbxGender.valueProperty().bindBidirectional(viewModel.genderProperty());
        cbxLanguage.valueProperty().bindBidirectional(viewModel.languageProperty());
        cbxLanguage.itemsProperty().bindBidirectional(viewModel.languagesProperty());
        cbxTitle.valueProperty().bindBidirectional(viewModel.titleProperty());
        cbxTitle.itemsProperty().bindBidirectional(viewModel.titlesProperty());
        cbxTitle2.valueProperty().bindBidirectional(viewModel.title2Property());
        cbxTitle2.itemsProperty().bindBidirectional(viewModel.titlesProperty());
        cbxTitle3.valueProperty().bindBidirectional(viewModel.title3Property());
        cbxTitle3.itemsProperty().bindBidirectional(viewModel.titlesProperty());
    }

    /**
     * initializes the ComboBox with the gender data
     */
    private void initCbxGender() {
        cbxGender.getItems().setAll(Gender.values());
    }

    /**
     * initializes the ComboBox with the languages data
     */
    private void initCbxLanguages() {

        String filePath = "lang.properties";
        try (InputStream input = new FileInputStream(filePath)) {

            // load the properties file and get the values
            Properties prop = new Properties();
            prop.load(Controller.class.getClassLoader().getResourceAsStream(filePath));
            Collection<String> propValues = (Collection<String>) (Collection<?>) prop.values();
            ObservableList<String> languages = FXCollections.observableArrayList(propValues);
            FilteredList<String> filterableLanguages = new FilteredList<>(languages, p -> true);
            makeComboboxFilterable(cbxLanguage, filterableLanguages, true);
            viewModel.setLanguages(filterableLanguages);
        } catch (IOException ex) {
            //assign empty list to combobox
            viewModel.setLanguages(FXCollections.observableArrayList());
        }
    }

    /**
     * initializes the ComboBox with the titles data
     */
    private void initCbxTitles() {
        ObservableList<String> titles = FXCollections.observableArrayList(dataRepository.getTitles());
        FilteredList<String> filterableTitles = new FilteredList<>(titles, p -> true);
        makeComboboxFilterable(cbxTitle, filterableTitles, false);
        makeComboboxFilterable(cbxTitle2, filterableTitles, false);
        makeComboboxFilterable(cbxTitle3, filterableTitles, false);
        viewModel.setTitles(filterableTitles);
    }

    /**
     * helper method to make a ComboBox filterable
     *
     * @param cbx               the ComboBox to be made filterable
     * @param filterableItems   data that should be shown in the ComboBox
     * @param forbidManualInput if true the user can only select supposed data, if false the user can manually type in anything
     */
    private void makeComboboxFilterable(ComboBox<String> cbx, FilteredList<String> filterableItems, boolean forbidManualInput) {
        cbx.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
            final TextField editor = cbx.getEditor();
            final String selected = cbx.getSelectionModel().getSelectedItem();
            Platform.runLater(() -> {
                if (selected == null || !selected.equals(editor.getText())) {
                    filterableItems.setPredicate(item -> {
                        if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            });
        });
        if (forbidManualInput) {
            cbx.setOnHiding((event -> {
                cbx.setEditable(false);
                cbx.setValue(cbx.getEditor().getText());
            }));
            cbx.setOnShowing((event -> {
                cbx.setEditable(true);
                cbx.getEditor().setText("");
                cbx.setValue(null);
            }));
        } else {
            cbx.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) cbx.show();
            });
        }
    }

    /**
     * clears all the current data shown in the GUI elements
     */
    public void clearFields() {
        txfContactInput.clear();
        txfSalutation.clear();
        txfStandardSalutation.clear();
        txfFirstName.clear();
        txfLastName.clear();
        cbxGender.setValue(null);
        cbxLanguage.setValue(null);
        cbxTitle.setValue(null);
        cbxTitle2.setValue(null);
        cbxTitle3.setValue(null);
    }

    /**
     * adds a textfield to the GUI for choosing a second title
     */
    public void addSecondTitleMenu() {
        cbxTitle2.setVisible(true);
        btnRemoveSecondTitle.setVisible(true);
        btnAddThirdTitleMenu.setVisible(true);
        RowConstraints rowConstraints10 = gridPane.getRowConstraints().get(10);
        rowConstraints10.setMaxHeight(Region.USE_COMPUTED_SIZE);
    }

    /**
     * adds a textfield to the GUI for choosing a third title
     */
    public void addThirdTitleMenu() {
        cbxTitle3.setVisible(true);
        btnRemoveThirdTitle.setVisible(true);
        RowConstraints rowConstraints11 = gridPane.getRowConstraints().get(11);
        rowConstraints11.setMaxHeight(Region.USE_COMPUTED_SIZE);
    }

    /**
     * removes the second title textField
     */
    public void removeSecondTitle() {
        cbxTitle2.setVisible(false);
        btnRemoveSecondTitle.setVisible(false);
        btnAddThirdTitleMenu.setVisible(false);
        viewModel.setTitle2(null);
        RowConstraints rowConstraints10 = gridPane.getRowConstraints().get(10);
        rowConstraints10.setMaxHeight(0);
    }

    /**
     * removes the second title textField
     */
    public void removeThirdTitle() {
        cbxTitle3.setVisible(false);
        btnRemoveThirdTitle.setVisible(false);
        viewModel.setTitle3(null);
        RowConstraints rowConstraints11 = gridPane.getRowConstraints().get(11);
        rowConstraints11.setMaxHeight(0);
    }

    /**
     * saves the contact data to the database
     */
    public void saveContact() {
        Contact contact = getContactFromGUI();
        //handle titles
        List<String> titleList = new ArrayList<>();
        if (viewModel.getTitle() != null && !viewModel.getTitle().trim().equals("")) {
            dataRepository.addTitle(viewModel.getTitle());
        }
        if (viewModel.getTitle2() != null && !viewModel.getTitle2().trim().equals("")) {
            dataRepository.addTitle(viewModel.getTitle2());
        }
        if (viewModel.getTitle3() != null && !viewModel.getTitle3().trim().equals("")) {
            dataRepository.addTitle(viewModel.getTitle3());
        }
        initCbxTitles();
        try {
            dataRepository.addContact(contact);
            showSuccessMessage("Kontakt wurde gespeichert");
            clearFields();

        } catch (ContactNotCompleteException e) {
            this.showFaultMessage("Bitte geben Sie eine Anrede, eine standardisierte Briefanrede, sowie einen Nachnamen an.");
        }
    }

    /**
     * Reads the data from the GUI fields and creates a Contact object holding the data
     *
     * @return the Contact object holding the data from the GUI
     */
    private Contact getContactFromGUI() {
        Contact contact = new Contact();
        contact.setSalutation(viewModel.getSalutation());
        contact.setStandardSalutation(viewModel.getStandardSalutation());
        contact.setFirstName(viewModel.getFirstName());
        contact.setLastName(viewModel.getLastName());
        contact.setGender(viewModel.getGender());
        contact.setLanguage(viewModel.getLanguage());
        List<String> titleList = new ArrayList<>();
        if (viewModel.getTitle() != null) titleList.add(viewModel.getTitle());
        if (viewModel.getTitle2() != null) titleList.add(viewModel.getTitle2());
        if (viewModel.getTitle3() != null) titleList.add(viewModel.getTitle3());
        contact.setTitleList(titleList);
        return contact;
    }

    /**
     * Takes the user input and gives it to the ContactSplitter.
     */
    public void splitContact() {
        String input = txfContactInput.getText();
        clearFields();
        txfContactInput.setText(input);
        Contact contact = null;
        try {
            contact = contactSplitter.splitContact(input);
            fillGUIwithData(contact);
        } catch (Exception e) {
            showFaultMessage("Kontakt konnte nicht erkannt werden");
        }
    }

    /**
     * Refreshes the GUI data when user makes changes
     */
    public void valueChanged() {
        Contact contact = getContactFromGUI();
        String standardSalutation = SalutationGenerator.generate(contact);
        viewModel.setStandardSalutation(standardSalutation);
    }

    /**
     * fills in contact data into the UI Elements
     *
     * @param contact Holds the data to be filled into the UI Elements
     */
    private void fillGUIwithData(Contact contact) {
        viewModel.setSalutation(contact.getSalutation());
        viewModel.setStandardSalutation(contact.getStandardSalutation());
        if (contact.getTitleList() != null) {
            if (contact.getTitleList().size() >= 1) viewModel.setTitle(contact.getTitleList().get(0));
            if (contact.getTitleList().size() >= 2) {
                viewModel.setTitle2(contact.getTitleList().get(1));
                addSecondTitleMenu();
            }
            if (contact.getTitleList().size() >= 3) {
                viewModel.setTitle3(contact.getTitleList().get(2));
                addThirdTitleMenu();
            }
        }
        viewModel.setNobilityTitle(contact.getNobilityTitle());
        viewModel.setGender(contact.getGender());
        viewModel.setFirstName(contact.getFirstName());
        viewModel.setLastName(contact.getLastName());
        viewModel.setLanguage(contact.getLanguage());
    }

    /**
     * Shows a message to the user if something went wrong
     *
     * @param message Message to be shown
     */
    private void showFaultMessage(String message) {
        Alert faultAlert = new Alert(Alert.AlertType.ERROR);
        faultAlert.setHeaderText(message);
        faultAlert.showAndWait();
    }

    /**
     * shows a message if the contact was successfully saved to the database
     *
     * @param message Message to be shown
     */
    private void showSuccessMessage(String message) {
        Alert successAlert = new Alert(Alert.AlertType.CONFIRMATION);
        successAlert.setHeaderText(message);
        successAlert.show();
    }
}
