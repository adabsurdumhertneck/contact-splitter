package data;

/**
 * Enum that holds all required genders
 */
public enum Gender {
    MALE, FEMALE, OTHER
}
