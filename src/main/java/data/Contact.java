package data;

import java.util.ArrayList;
import java.util.List;

public class Contact {
    private String salutation;
    private String standardSalutation;
    private String title;
    private List<String> titleList = new ArrayList<>();
    private String nobilityTitle;
    private Gender gender;
    private String firstName;
    private String lastName;
    private String language;

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getStandardSalutation() {
        return standardSalutation;
    }

    public void setStandardSalutation(String standardSalutation) {
        this.standardSalutation = standardSalutation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<String> titleList) {
        this.titleList = titleList;
    }

    public String getNobilityTitle() {
        return nobilityTitle;
    }

    public void setNobilityTitle(String nobilityTitle) {
        this.nobilityTitle = nobilityTitle;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
