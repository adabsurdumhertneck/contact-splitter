package data.sqlite;

import java.sql.Connection;

public class ContactNotCompleteException extends Exception {

    public ContactNotCompleteException() {
        super();
        System.out.println("Not all mandatory fields are filled in the contact object.");
    }

}
