package data.sqlite;

import data.Contact;
import data.IDataRepository;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * Repository used to communicate with a SQLite database.
 */
public class SQLiteRepository implements IDataRepository {

    private final String databaseURL = "jdbc:sqlite:db:contact-splitter.db";
    private static SQLiteRepository instance;
    private Connection connection;

    private SQLiteRepository() {

        try {
            this.connect();
            System.out.println("Initial connection successful.");

            String sqlCreateStatement = "CREATE TABLE IF NOT EXISTS contacts (\n"
                    + " id integer PRIMARY KEY,\n"
                    + " firstName text,\n"
                    + " lastName text NOT NULL,\n"
                    + " gender text,\n"
                    + " language text,\n"
                    + " salutation text NOT NULL,\n"
                    + " title text,\n"
                    + " nobilityTitle text,\n"
                    + " standardSalutation text NOT NULL\n"
                    + ");";

            try {
                Statement statement = connection.createStatement();
                statement.execute(sqlCreateStatement);
                System.out.println("Contacts table initialized.");
            } catch (SQLException e) {
                System.out.println("Contacts table couldn't be initialized.");
                e.printStackTrace();
            }

            sqlCreateStatement = "CREATE TABLE IF NOT EXISTS titles (\n"
                    + " id integer PRIMARY KEY,\n"
                    + " title text\n"
                    + ");";

            try {
                Statement statement = connection.createStatement();
                statement.execute(sqlCreateStatement);
                System.out.println("Titles table initialized.");
            } catch (SQLException e) {
                System.out.println("Titles table couldn't be initialized.");
                e.printStackTrace();
            }

        } catch (SQLException e) {
            System.out.println("Initial connection unsuccessful.");
            e.printStackTrace();
        }

    }

    /**
     * Establishes the connection with the SQLite database at the correct path.
     * @throws SQLException Exception that occurs, when the connection to the database could not be established
     */
    private void connect() throws SQLException {
        connection = DriverManager.getConnection(this.databaseURL);
    }

    /**
     * Returns the instance of this class, which will be created if necessary.
     * @return Singleton instance of this class
     */
    public static SQLiteRepository getInstance() {
        if (instance == null) {
            instance = new SQLiteRepository();
        }
        return instance;
    }

    /**
     * {@inheritDoc}
     * @param contact Contact-object that has to contain at least the lastname, the salutation, and the standardSalutation
     * @throws ContactNotCompleteException Exception that occurs when not all mandatory fields are filled in the contact object
     */
    public void addContact(Contact contact) throws ContactNotCompleteException {

        if (contact.getLastName() != null && contact.getSalutation() != null && contact.getStandardSalutation() != null) {

            try {

                this.connect();
                System.out.println("Connection successful.");

                String sqlInsertStatement = "INSERT INTO contacts(firstName,lastName,gender,language,salutation,title,nobilityTitle,standardSalutation) VALUES(?,?,?,?,?,?,?,?);";

                try {

                    PreparedStatement preparedStatement = connection.prepareStatement(sqlInsertStatement);

                    preparedStatement.setString(1, contact.getFirstName());
                    preparedStatement.setString(2, contact.getLastName());
                    if (contact.getGender() != null) {
                        preparedStatement.setString(3, contact.getGender().toString().toLowerCase());
                    } else {
                        preparedStatement.setString(3, null);
                    }
                    if (contact.getLanguage() != null) {
                        preparedStatement.setString(4, contact.getLanguage().toString().toLowerCase());
                    } else {
                        preparedStatement.setString(4, null);
                    }
                    preparedStatement.setString(5, contact.getSalutation());
                    preparedStatement.setString(6, contact.getTitle());
                    preparedStatement.setString(7, contact.getNobilityTitle());
                    preparedStatement.setString(8, contact.getStandardSalutation());

                    preparedStatement.executeUpdate();

                    System.out.println("Contact added.");

                } catch (SQLException e) {
                    System.out.println("Contact couldn't been added.");
                    e.printStackTrace();
                }

                try {
                    this.connection.close();
                    System.out.println("Connection closed.");
                } catch (SQLException e) {
                    System.out.println("Connection couldn't be closed.");
                    e.printStackTrace();
                }

            } catch (SQLException e) {
                System.out.println("Connection unsuccessful.");
                e.printStackTrace();
            }

        } else {
            throw new ContactNotCompleteException();
        }

    }

    /**
     * {@inheritDoc}
     * @param title New title
     */
    public void addTitle(String title) {

        if (!this.getTitles().contains(title)) {

            try {

                this.connect();
                System.out.println("Connection successful.");

                String sqlInsertStatement = "INSERT INTO titles(title) VALUES(?);";

                try {

                    PreparedStatement preparedStatement = connection.prepareStatement(sqlInsertStatement);

                    preparedStatement.setString(1, title);

                    preparedStatement.executeUpdate();

                    System.out.println("Title added.");

                } catch (SQLException e) {
                    System.out.println("Title couldn't be added.");
                    e.printStackTrace();
                }

                try {
                    this.connection.close();
                    System.out.println("Connection closed.");
                } catch (SQLException e) {
                    System.out.println("Connection couldn't be closed.");
                    e.printStackTrace();
                }

            } catch (SQLException e) {
                System.out.println("Connection unsuccessful.");
                e.printStackTrace();
            }

        }

    }

    /**
     * {@inheritDoc}
     * @return List of titles
     */
    public List<String> getTitles() {

        List<String> titles = new ArrayList<String>();

        try {
            this.connect();
            System.out.println("Connection successful.");

            String sqlSelect = "SELECT title FROM titles";

            try {
                Statement statement = connection.createStatement();
                ResultSet results = statement.executeQuery(sqlSelect);

                while (results.next()) {
                    titles.add(results.getString("title"));
                }

                System.out.println("Titles red.");

            } catch (SQLException e) {
                System.out.println("Titles couldn't be red.");
                e.printStackTrace();
            }

        } catch (SQLException e) {
            System.out.println("Connection unsuccessful.");
            e.printStackTrace();
        }

        return titles;

    }
}
