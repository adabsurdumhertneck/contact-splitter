package data;

import data.sqlite.ContactNotCompleteException;

import java.util.List;

/**
 * Interface to communicate with a database.
 */
public interface IDataRepository {

    /**
     * Adds the attributes of a Contact-object to the contacts-table in the database.
     * @param contact Contact-object that has to contain at least the lastname, the salutation, and the standardSalutation
     * @throws ContactNotCompleteException Exception that occurs when not all mandatory fields are filled in the contact object
     */
    void addContact(Contact contact) throws ContactNotCompleteException;

    /**
     * Adds a title to the Titles-table.
     * @param title New title
     */
    void addTitle(String title);

    /**
     * Returns a list of all titles currently in the database.
     * @return List of titles
     */
    List<String> getTitles();

}
