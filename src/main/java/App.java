import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

/**
 * The JavaFX Application
 * @author Robert Metzinger
 */
public class App extends Application {

    /**
     * launches the app
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Loads and shows the GUI
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        String filePath = "/frontend/GUI.fxml";
        //String filePath = File.separator + "frontend" + File.separator + "GUI.fxml";
        System.out.println(filePath);
        Parent root = FXMLLoader.load(getClass().getResource(filePath));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }
}
